import React,{Component} from 'react';
import queryString from 'query-string';
import LeftPanel from './leftPannel';
class Employees extends Component{
    onChangeOptions=(options)=>{
        let {loc}=this.props.match.params;
        console.log(options);
        this.callUrl(`/emps/${loc}`,options);
    }
    callUrl=(url,options)=>{
        let searchStr=this.makeSearchStr(options);
        //console.log(searchStr);
        this.props.history.push({
            pathName:url,
            search:searchStr,
        }
        )
    }
    makeSearchStr=(options)=>{
        let {department,designation}=options;
        let searchStr="";
        searchStr=this.addToQueryString(searchStr,"department",department);
        searchStr=this.addToQueryString(searchStr,"designation",designation);
        return searchStr;
    }
    addToQueryString=(searchStr,name,value)=>{
        return value?
            searchStr?`${searchStr}&${name}=${value}`
            :`${name}=${value}`
            :searchStr;
    }
    changePage=(num,pageNum,searchStr)=>{
        let {loc}=this.props.match.params;
        num===1?pageNum++:pageNum--;
        this.props.history.push({
            pathName:`/ems/${loc}`,
            search:searchStr?`${searchStr}&pageNo=`+pageNum:`pageNo=`+pageNum
        });
    }
    render(){
        let {loc}=this.props.match.params;
        let {emps}=this.props;
        emps=loc?emps.filter((x)=>x.location===loc):emps;
        let queryParams=queryString.parse(this.props.location.search);
        let searchStr=this.makeSearchStr(queryParams);
        let {designation,department,pageNo=1}=queryParams;
        emps=department?emps.filter((x)=>department.split(",").find((x1)=>x1===x.department)):emps;
        emps=designation?emps.filter((x)=>x.designation===designation):emps;
        let pageNum=+pageNo;
        console.log('PageNo:'+pageNo);
        let pageSize=2;
        let firstIndex=(pageNum-1)*pageSize;
        let lastIndex=firstIndex+pageSize-1
        let emps1=emps.filter((x,index)=> index>=firstIndex&&index<=lastIndex)
        console.log("first index:"+firstIndex+"\nlast index:"+lastIndex);
        return(
            <div className="container p-2">
                <div className="row">
                    <div className="col-4 bg-light border-left">
                        <LeftPanel
                        options={queryParams}
                        onOptionChange={this.onChangeOptions}/>
                    </div>
                    <div className="col-8 pl-4">
                        <h3 className="text-center">Welcome to Employee Portal</h3>
                        <b>You Have Chosen</b><br/>
                        Location :{loc?loc:"All"}<br/>
                        Department:{department?department:"All"}<br/>
                        Designation:{designation?designation:"All"}<br/><br/>
                        The number of employees matching the options {emps.length}
                        <div className="row">
                        {
                        emps1.map((x,index)=>(
                                <div className="col-6 border" key={index}>
                                    <b>{x.name}</b><br/>
                                    {x.email}<br/>
                                    Mobile:{x.mobile}<br/>
                                    Location:{x.location}<br/>
                                    Department:{x.department}<br/>
                                    Designation:{x.designation}<br/>
                                    Salary:{x.salary}<br/>
                                </div>
                        ))
                    }
                    </div>
                    <div>
                    <span className="float-left">{pageNo<=1?"":<button  className="btn btn-primary" onClick={()=>this.changePage(0,pageNo,searchStr)}>Previous</button>}</span>
                    <span className="float-right">{lastIndex<emps.length-1?<button className="btn btn-primary" onClick={()=>this.changePage(1,pageNo,searchStr)}>Next</button>:""}</span>
                   </div>
                    </div>
                    
                </div>
            </div>
        )

    }
}
export default Employees;