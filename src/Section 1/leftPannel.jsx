import React,{Component} from 'react';
class LeftPanel extends Component{
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let options={...this.props.options};
        input.type==="checkbox"
            ?options[input.name]=this.updateCBs(options[input.name],input.checked,input.value)
            :options[input.name]=input.value;
        this.props.onOptionChange(options);
    }
    updateCBs=(inpVal,checked,value)=>{
        let inpArr=inpVal?inpVal.split(","):[];
        if(checked){
            inpArr.push(value);
        }
        else{
            let index=inpArr.findIndex((x)=>x===value);
            inpArr.splice(index,1);
        }
        return inpArr.join(",");
    }
    makeCheckboxes=(arr,values,name,label)=><React.Fragment>
            <label className="form-check-lebel font-weight-bold">{label}</label>
            {
                arr.map((x,index)=>(
                    <div className="form-check" key={index}>
                        <input
                        className="form-check-input"
                        value={x}
                        type="checkbox"
                        id={x}
                        key={index}
                        name={name}
                        checked={values.find((x2)=>x2===x)||false}
                        onChange={this.handleChange}/>
                        <label className="form-check-label" htmlFor={x}>{x}</label>
                    </div>
                    
                ))
            }
        </React.Fragment>
        makeRadio=(arr,value,name,label)=><React.Fragment>
        <label className="form-check-lebel font-weight-bold">{label}</label>
        {
            arr.map((x,index)=>(
                <div className="form-check" key={index}>
                    <input
                    className="form-check-input"
                    value={x}
                    type="radio"
                    id={x}
                    key={index}
                    name={name}
                    checked={value===x}
                    onChange={this.handleChange}/>
                    <label className="form-check-label" htmlFor={x}>{x}</label>
                </div>
                
            ))
        }
    </React.Fragment>
    render(){
        let {department="",designation=""}=this.props.options;
        let departmentArr=["Finance","Technology","Operations","HR"];
        let designationArr=["Manager","Trainee","Presedent"];
        return(<React.Fragment>
            <div className="row">
                <div className="col-12">
                    {this.makeRadio(designationArr,designation,"designation","Designation")}
                </div>
                <div className="col-12">
                    {this.makeCheckboxes(departmentArr,department.split(","),"department","Department")}
                </div>
            </div>
        </React.Fragment>)
    }
}
export default LeftPanel;